/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#include <map>

int send_meps_mpi(std::map<std::string, std::string> const& allen_options);
