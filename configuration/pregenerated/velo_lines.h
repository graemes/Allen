/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

#include <tuple>
#include "../../device/selections/Hlt1/include/LineTraverser.cuh"

using configured_lines_t = std::tuple<>;
