/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#pragma once

namespace Hlt1 {
  // Types of lines
  struct Line {};

  struct SpecialLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct VeloLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct OneTrackLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct TwoTrackLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct VeloUTTwoTrackLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct ThreeTrackLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
  struct FourTrackLine : Line {
    constexpr static auto scale_factor = 1.f;
  };
} // namespace Hlt1
