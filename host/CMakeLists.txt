###############################################################################
# (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      #
###############################################################################
add_subdirectory(data_provider)
add_subdirectory(global_event_cut)
add_subdirectory(velo/clustering)
add_subdirectory(prefix_sum)
add_subdirectory(init_event_list)